﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class PathMovement : MonoBehaviour {

	public float moveSpeed = 5.0f;

	public bool retracePath = true,
				drawPath = false,
				enabledByButton = false;

	private GameObject pathPoints;

	private Vector2 initialPosition;
	private Quaternion initialRotation;

	[SerializeField]
	protected List<Point> points;

	protected List<Edge> edges;
	
	[Serializable]
	public class Point {
		
		public Point (Vector2 p) {

			this.position = p;
			edges = new List<Edge>();
		}
		
		public void AddEdge (Edge e) {
			edges.Add (e);
		}
		
		public float x { get { return position.x; } }
		public float y { get { return position.y; } }
		
		public Vector2 position;

		[HideInInspector]
		public List<Edge> edges;
	}

	[Serializable]
	public class Edge {
		
		public Edge (Point p1, Point p2) {

			this.p1 = p1;
			this.p2 = p2;
			p1.AddEdge (this);
			p2.AddEdge (this);
		}
		
		public Point p1;
		public Point p2;
	}

	void Awake () {

		edges = new List<Edge>();

		initialPosition = this.transform.position;
		initialRotation = this.transform.rotation;

		BuildEdgeList ();
		
		if (drawPath) 
			InstantiatePathPoints ();
	}

	void Init () {

		initialPosition = this.transform.position;
				
		StartCoroutine (TraversePath(edges[0]));
	}

	void OnEnable () {

		if (enabledByButton)
			Init ();
	}

	void OnDisable () {
		
		StopAllCoroutines ();
	}

	void OnLevelStart () {

		Destroy (pathPoints);

		if (!enabledByButton)
			Init ();
	}

	void OnLevelRestart () {

		this.transform.position = initialPosition;

		StopAllCoroutines ();

		if (drawPath) 
			InstantiatePathPoints ();
	}

	void InstantiatePathPoints () {

		pathPoints = new GameObject();
		pathPoints.name = "PathPoints";
		pathPoints.transform.parent = this.transform;

		Quaternion playerAddedRotation = transform.rotation * Quaternion.Inverse (initialRotation);

		foreach (Edge edge in edges) {

			GameObject p1 = Instantiate (Resources.Load ("Prefabs/PathPoint")) as GameObject;
			p1.transform.position = this.transform.position + playerAddedRotation * edge.p1.position;
			p1.transform.parent = pathPoints.transform;

			GameObject p2 = Instantiate (Resources.Load ("Prefabs/PathPoint")) as GameObject;
			p2.transform.position = this.transform.position + playerAddedRotation * edge.p2.position;
			p2.transform.parent = pathPoints.transform;
		}
	}

	protected void BuildEdgeList () {

		for (int i = 0; i < points.Count-1; i++) {

			edges.Add (new Edge (points[i], points[i+1]));
		}
	}

	protected virtual IEnumerator TraversePath (Edge currentEdge) {
		
		while (true) {

			// move to next vertex in current edge
			yield return StartCoroutine (TraverseEdge(currentEdge));

			// update current edge
			List<Edge> nextEdge = currentEdge.p2.edges.Except (currentEdge.p1.edges).ToList ();

			// backtrack along path if reached end
			if (nextEdge.Count == 0) {

				if (retracePath) {

					// swap edge endpoints
					foreach (Edge edge in edges) {

						Point temp = edge.p1;
						edge.p1 = edge.p2;
						edge.p2 = temp;
					}
				}

				else {

					this.enabled = false;
				}
			}

			else {

				currentEdge = nextEdge[0];
			}
		}
	}
	
	protected IEnumerator TraverseEdge (Edge edge) {

		Vector2 p1 = transform.rotation * Quaternion.Inverse (initialRotation) * edge.p1.position;
		Vector2 p2 = transform.rotation * Quaternion.Inverse (initialRotation) * edge.p2.position;
		
		Vector2 direction = p2 - p1;

		while (true) {
			
			float moveDistance = moveSpeed * Time.deltaTime;
			Vector2 toDestination = initialPosition + p2 - (Vector2) transform.position;
			
			if (Vector2.Dot (direction, toDestination) < 0) {

				yield break;
			}
			
			else {

				transform.Translate (direction.normalized * moveDistance, Space.World);
				yield return null;
			}
		}
	}
}
