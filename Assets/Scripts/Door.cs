﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Door : MonoBehaviour {

	List<GameObject> friendsArrived;

	void Awake () {

		friendsArrived = new List<GameObject>();
	}

	void OnTriggerEnter2D (Collider2D col) {

		if (col.CompareTag ("Friend")) {

			col.gameObject.SetActive (false);

			friendsArrived.Add (col.gameObject);

			List<GameObject> friends = GameObject.FindGameObjectsWithTag ("Friend").Cast<GameObject>().ToList ();
			List<GameObject> friendsLeft = friends.Except (friendsArrived).ToList ();
			
			// level over
			if (friendsLeft.Count == 0) {

				StartCoroutine (LevelComplete ());
			}
		}
	}

	IEnumerator LevelComplete () {

		yield return new WaitForSeconds (1.5f);

		GameManager.LoadNextLevel ();
	}

	void OnLevelRestart () {

		foreach (GameObject friend in friendsArrived) {

			friend.SetActive (true);

			Friend script = friend.GetComponent(typeof(Friend)) as Friend;
			script.OnLevelRestart ();

			friend.GetComponent<PlatformerMovement>().enabled = false;
		}

		friendsArrived.Clear ();
	}
}
