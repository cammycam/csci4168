﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject background;			// needs background to automatically assign bounds

	public float zoomSpeed,
				 minOrthoSize,
				 maxOrthoSize;

	private static float responseRadius = 0.3f,			// dist from viewport centre that mouse starts moving cam
				 		 moveSpeed = 12;
	
	private float initialOrthoSize,
				  deltaTime,
				  prevTimeSample;

	private Bounds bounds;

	void Awake () {

		initialOrthoSize = Camera.main.orthographicSize;

		bounds = background.GetComponent<SpriteRenderer>().bounds;
	}

	void LateUpdate () {

		// keep own deltatime measurement so can pause using timescale = 0
		deltaTime = Time.realtimeSinceStartup - prevTimeSample;
		prevTimeSample = Time.realtimeSinceStartup;

		UpdateZoom ();
		UpdatePosition ();
		ClampPosition ();
	}

	// zooms camera w/ mouse wheel
	void UpdateZoom () {

		float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
		
		if (mouseWheel != 0f) {

			float orthoSize = Camera.main.orthographicSize -= mouseWheel * zoomSpeed;
			float clampedZoom = Mathf.Clamp (orthoSize, minOrthoSize, maxOrthoSize );

			Camera.main.orthographicSize = clampedZoom;
		}
	}

	// scrolls camera when mouse between edge of camera trigger collider and edge of screen
	void UpdatePosition () {

		// world coords
		Vector2 mousePosition = Camera.main.ScreenToViewportPoint (Input.mousePosition);
		Vector2 viewportCenter = new Vector2 (0.5f, 0.5f);

		float distFromViewportCenter = Vector2.Distance (mousePosition, viewportCenter);

		if (distFromViewportCenter > responseRadius) {

			// speed increases with dist from response radius (if outside of)
			float speed = Mathf.Min (1.0f, (distFromViewportCenter - responseRadius) / responseRadius) * moveSpeed;

			Vector2 translation = (mousePosition - viewportCenter).normalized * speed * deltaTime;

			transform.Translate (translation);
		}
	}

	void ClampPosition () {

		Vector3 pos = transform.position;

		float heightOffset = Camera.main.orthographicSize;
		float widthOffset = heightOffset * Camera.main.aspect;

		pos.x = Mathf.Clamp (pos.x, bounds.min.x + widthOffset, bounds.max.x - widthOffset);
		pos.y = Mathf.Clamp (pos.y, bounds.min.y + heightOffset, bounds.max.y - heightOffset);

		transform.position = pos;
	}

	public IEnumerator ResetZoom () {
		
		while (Camera.main.orthographicSize != initialOrthoSize) {
			
			Camera.main.orthographicSize = Mathf.MoveTowards (Camera.main.orthographicSize, initialOrthoSize, 100f * zoomSpeed * deltaTime);
			
			yield return null;
		}
	}
}
