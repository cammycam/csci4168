﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	int buttonH, buttonW;
	public GUISkin customSkin;
	public GUIText title;

	int buttonX;

	// Use this for initialization
	void Start () {
	}

	void Update () {
		buttonH = Screen.height/6;
		buttonW = Screen.width/4;

		buttonX = Screen.width / 2 - buttonW / 2;
	}
	
	// Update is called once per frame
	void OnGUI () {
		GUI.skin = customSkin;

		int titleSize = Screen.height / 20;
		int textSize = Screen.height / 42;

		GUI.Label (new Rect (0, titleSize, Screen.width, Screen.height/3), "<size=" + titleSize + ">Super Shepherd Simulator</size>");

		if(GUI.Button(new Rect(buttonX,Screen.height/2 - 60,buttonW,buttonH), "<size=" + textSize +">Level Select</size>")) {
			Application.LoadLevel ("LevelSelect"); 
		}

		if(GUI.Button(new Rect(buttonX,Screen.height/2 - 40 + buttonH,buttonW,buttonH), "<size=" + textSize +">Instructions</size>")) {
			Application.LoadLevel ("Instructions"); 
		}

		if(GUI.Button(new Rect(buttonX,Screen.height/2 - 20 + (buttonH * 2),buttonW,buttonH), "<size=" + textSize +">Quit</size>")) {
			Application.Quit();
		}
	}
}
