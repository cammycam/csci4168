﻿using UnityEngine;
using System.Collections;

public class Instructions : MonoBehaviour {
	public GUISkin customSkin;
	int buttonW;
	int buttonH;

	// Use this for initialization
	void Start () {
	}

	void Update(){
		buttonH = Screen.height/6;
		buttonW = Screen.width/6;
	}

	void OnGUI () {
		GUI.skin = customSkin;

		int titleSize = Screen.height / 20;
		int textSize = Screen.height / 45;

		GUI.Label (new Rect (0, titleSize, Screen.width, Screen.height/3), "<size=" + titleSize + ">Instructions</size>");
		
		if(GUI.Button(new Rect(10,10,buttonW,buttonH), "<size=" + textSize + ">Back</size>")) {
			Application.LoadLevel("MainMenu");
		}

		
		GUI.Label (new Rect (Screen.width/6, Screen.height/4, Screen.width*2/3, Screen.height* 3/4), 
		           "<size=" + textSize + ">It's up to you to save all of the cute little creatures! " +
		           "Before the level starts, use your mouse to rearrange the flashing blocks so that the animals " +
		           "can get to the door at the end of the level." +
		           "</size>\n\n" +
		           "<size=" + (textSize + 4)+ ">Controls</size>\n" +
		           "<size=" + textSize + ">Left-Click - pick up/drop movable elements\n" +
		           "Right-click - undo\n" +
		           "A & D - Rotate block\n" +
		           "R - Restart level\n" +
		           "Q - Return to main menu" +
		           "</size>");
	}
}
