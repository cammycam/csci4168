﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Jello : MonoBehaviour {

	public float bounceVelocity = 30f;

	private Animator anim;

	void Awake () {

		anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D (Collider2D col) {
		
		if (col.CompareTag ("Friend")) {
			
			anim.SetTrigger ("jiggle");
		}
	}
}
