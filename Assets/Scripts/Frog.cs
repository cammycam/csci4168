﻿using UnityEngine;
using System.Collections;

public class Frog : Friend {

	public float timeBetweenJumps = 1.5f;

	public Vector2 jumpVelocity;

	private bool isOnJello;

	// Use this for initialization
	protected override void Start () {
	
		base.Start ();
	}

	public override void OnLevelRestart () {

		base.OnLevelRestart ();
	
		StopCoroutine ("Jump");
	}
	
	IEnumerator Jump () {

		// wait for jump time
		yield return new WaitForSeconds (timeBetweenJumps);

		movement.Velocity = new Vector2 (transform.right.x * jumpVelocity.x, jumpVelocity.y);
		
		animator.SetBool ("isJumping", true);	
	}

	void OnGrounded () {

		movement.Velocity = new Vector2 (0f, movement.Velocity.y);
	
		animator.SetBool ("isJumping", false);

		StartCoroutine ("Jump");
	}

	protected override void OnTriggerEnter2D (Collider2D col) {

		if (col.CompareTag ("Jello")) {

			movement.Velocity = new Vector2 (transform.right.x * jumpVelocity.x, col.GetComponent<Jello>().bounceVelocity);
		}
	}
}
