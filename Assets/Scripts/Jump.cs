﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlatformerMovement))]
public class Jump : MonoBehaviour {

	[SerializeField]
	protected float speed,
			   		acceleration,
					accelerationDuration;

	private PlatformerMovement movement;

	private bool isActive,
				 isAccelerating;

	public float AccelerationDuration {

		get { return accelerationDuration; }
	}
	
	void Awake () {

		movement = GetComponent<PlatformerMovement>();
	}

	public bool Do () {

		bool triggered = false;

		if (!isActive) {
		
			isActive = true;

			movement.Velocity = new Vector2 (0, speed);

			if (accelerationDuration > 0f) {
				StartCoroutine (AddAcceleration());
			}

			StartCoroutine (ListenForGrounded());

			triggered = true;
		}

		return triggered;
	}

	// reset jump when grounded
	private IEnumerator ListenForGrounded() {

		yield return null;
		
		while (!movement.IsGrounded) {
			yield return null;
		}

		isActive = false;
	}

	public void EndJumpAcceleration () {

		isAccelerating = false;
	}

	private IEnumerator AddAcceleration () {

		isAccelerating = true;
		
		yield return null;

		float endTime = Time.realtimeSinceStartup + accelerationDuration;
		
		while ( isAccelerating && Time.realtimeSinceStartup < endTime ) {

			Vector2 velocity = movement.Velocity;
			velocity.y += acceleration * Time.deltaTime;
			movement.Velocity = velocity;

			yield return null;
		}

		isAccelerating = false;
	}	
}
