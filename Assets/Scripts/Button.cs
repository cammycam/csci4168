﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	public enum EventType { enable, disable }

	public EventType eventType;

	[SerializeField]
	protected Sprite upButtonSprite,
					 downButtonSprite;

	[SerializeField]
	protected MonoBehaviour script;		// script that button activates

	[SerializeField]
	protected bool reverseOnButtonUp;			// if true, reverse disabling/enabling of script when down button returns to up position

	private int numObjectsTriggering;			// tracks number of objects currently holding button down

	void OnLevelRestart () {

		GetComponent<SpriteRenderer>().sprite = upButtonSprite;

		if (eventType == EventType.enable) {
			script.enabled = false;
		}
		
		else if (eventType == EventType.disable) {
			script.enabled = true;
		}

		numObjectsTriggering = 0;
	}

	void OnTriggerEnter2D (Collider2D col) {

		if (col == null || col.CompareTag ("Friend")) {

			if (numObjectsTriggering++ == 0) {

				GetComponent<SpriteRenderer>().sprite = downButtonSprite;
				
				if (eventType == EventType.enable) {
					script.enabled = true;
				}
				
				else if (eventType == EventType.disable) {
					script.enabled = false;
				}
			}
		}
	}

	void OnTriggerExit2D (Collider2D col) {

		if (col == null || col.CompareTag ("Friend")) {

			if (--numObjectsTriggering <= 0) {

				numObjectsTriggering = 0;

				if (reverseOnButtonUp) {
				
					GetComponent<SpriteRenderer>().sprite = upButtonSprite;

					if (eventType == EventType.enable) {
						script.enabled = false;
					}

					else if (eventType == EventType.disable) {
						script.enabled = true;
					}
				}
			}
		}
	}

	void OnLaserEnter () {

		OnTriggerEnter2D (null);
	}

	void OnLaserExit () {
		
		OnTriggerExit2D (null);
	}
}
