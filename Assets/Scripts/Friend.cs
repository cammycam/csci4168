﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PlatformerMovement))]
[RequireComponent(typeof(Animator))]
public class Friend : MonoBehaviour {
	
	private Vector2 initialPosition;
	private Quaternion initialRotation;

	protected PlatformerMovement movement;
	protected Animator animator;

	protected virtual void Start () {
	
		initialPosition = this.transform.position;
		initialRotation = this.transform.rotation;

		movement = GetComponent<PlatformerMovement>();
		animator = GetComponent<Animator>();
	}

	public virtual void OnLevelRestart () {

		this.transform.position = initialPosition;
		this.transform.rotation = initialRotation;

		movement.Velocity = Vector2.zero;

		StopAllCoroutines ();

		animator.enabled = true;
		
		renderer.material.shader = Shader.Find ("Sprites/Default");
	}
	
	void Update () {

		if (transform.position.y < -50) {

			GameManager.RestartLevel ();
		}
	}

	protected virtual void OnTriggerEnter2D (Collider2D col) {

		if (col.CompareTag ("Jello")) {

			// set friend y-velocity to jello bounce velocity
			Vector2 friendVelocity = movement.Velocity;
			friendVelocity.y = col.GetComponent<Jello>().bounceVelocity;
			movement.Velocity = friendVelocity;
		}
	}

	void OnLaserEnter () {

		StartCoroutine (LaserDeath());
	}

	IEnumerator LaserDeath () {

		animator.enabled = false;
		movement.enabled = false;

		SpriteRenderer rend = GetComponent<SpriteRenderer>();
		rend.material.shader = Shader.Find ("Custom/Disintegrate");

		int width = 16;
		int height = 16;

		Texture2D tex = new Texture2D (width, height);
		tex.filterMode = FilterMode.Point;
		tex.wrapMode = TextureWrapMode.Clamp;

		rend.material.SetTexture ("_AlphaTex", tex);

		Color[] pixels = new Color[width*height];

		// initialize alphas to 1
		for (int i = 0; i < pixels.Length; i++) {
			pixels[i] = new Color (0,0,0,1);
		}

		// initialize random index array
		int[] indices = new int[pixels.Length];
		for (int i = 0; i < pixels.Length; i++) {
			indices[i] = i;
		}

		Permute (indices);

		for (int i = 0; i < pixels.Length; i += 3) {

			pixels[indices[i]].a = 0f;

			if (i+1 < pixels.Length)
				pixels[indices[i+1]].a = 0f;
			if (i+2 < pixels.Length)
				pixels[indices[i+2]].a = 0f;

			tex.SetPixels (pixels);
			tex.Apply ();

			yield return null;
		}

		animator.enabled = true;

		rend.material.shader = Shader.Find ("Sprites/Default");

		GameManager.RestartLevel ();
	}

	void Permute <T>(IList<T> array) {

		for (int i = 0; i < 10 * array.Count; i++) {

			int ind1 = i % array.Count;
			int ind2 = Random.Range (0, array.Count-1);

			T temp = array[ind1];
			array[ind1] = array[ind2];
			array[ind2] = temp;
		}
	}
}
