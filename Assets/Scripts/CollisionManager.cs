﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class CollisionManager : MonoBehaviour {

	[SerializeField]
	protected bool ignoreSelf,
				   ignoreTriggers;

	[SerializeField]
	protected int numRays;

	[SerializeField]
	protected float rayMargin;				// inset ray from edge of bounding box so no x-axis collisions are triggered when on ground etc.

	[SerializeField]
	protected LayerMask mask;

	private RaycastHit2D[] hitAlloc;		// array for raycastNonAlloc

	private int hitAllocSize = 5;

	void Awake () {

		hitAlloc = new RaycastHit2D [hitAllocSize];
	}

	// wrappers for providing no layermask
	public RaycastHit2D CheckCollision ( Vector2 direction, float distance, Bounds bounds, out float closestHitDistance ) {

			return CheckCollision ( direction, distance, bounds, mask, out closestHitDistance );
	}

	public int CheckCollision ( Vector2 direction, float distance, Bounds bounds, List<RaycastHit2D> hitInfos, out float closestHitDistance ) {

		return CheckCollision ( direction, distance, bounds, mask, hitInfos, out closestHitDistance );
	}

	// if collision, returns first hit and outputs closestHitDistance
	protected RaycastHit2D CheckCollision ( Vector2 direction, float distance, Bounds bounds, LayerMask layerMask, out float closestHitDistance ) {

		// ray sweep start/end locations - add/subtract rayMargin so no casts from collider edges stop valid movement
		Vector2 startPoint, endPoint;

		// dist from ray origin to own collider edge
		float boundsMargin;
				
		// cast in x direction
		if ( direction.x != 0f ) {
			
			startPoint = new Vector2 ( bounds.center.x, bounds.min.y + rayMargin );
			endPoint = new Vector2 ( bounds.center.x, bounds.max.y - rayMargin );
			boundsMargin = bounds.size.x / 2f;
		}
		
		// y cast
		else {
			
			startPoint = new Vector2 ( bounds.min.x + rayMargin, bounds.center.y );
			endPoint = new Vector2 ( bounds.max.x - rayMargin, bounds.center.y );
			boundsMargin = bounds.size.y / 2f;
		}

		// ray starts from collider center so add half of bounds size
		float castDistance = distance + boundsMargin;

		closestHitDistance = int.MaxValue;
		
		RaycastHit2D closestHit = default (RaycastHit2D);
		
		// cast each ray
		for ( int i = 0; i < numRays; i++ ) {
			
			float lerpFactor = i / ( (float) (numRays - 1) );
			
			Vector2 origin = Vector2.Lerp ( startPoint, endPoint, lerpFactor );
			
			int numHits = Physics2D.RaycastNonAlloc ( origin, direction, hitAlloc, castDistance, layerMask );

			// iterate over hit objects
			for ( int j = 0; j < numHits; j++ ) {

				RaycastHit2D hit = hitAlloc[j];

				// hack
				if (hit.collider.CompareTag ("Jello"))
					continue;

				if ( (ignoreTriggers && hit.collider.isTrigger) || (ignoreSelf && (hit.transform == this.transform)) ) 
					continue;

				// update closest hit
				float hitDistance = (castDistance * hit.fraction) - boundsMargin;

				if (hitDistance < closestHitDistance) {
					closestHitDistance = hitDistance;
					closestHit = hit;
				}
			}
		}
		
		return closestHit;
	}

	// if collision, returns num colliders hit and fills given hitInfos list/outputs closestHitDistance
	protected int CheckCollision ( Vector2 direction, float distance, Bounds bounds, LayerMask layerMask, List<RaycastHit2D> hitInfos, out float closestHitDistance ) {

		// ray sweep start/end locations - add/subtract rayMargin so no casts from collider edges stop valid movement
		Vector2 startPoint, endPoint;
	
		// dist from ray origin to own collider edge
		float boundsMargin;

		// cast in x direction
		if ( direction.x != 0f ) {

			startPoint = new Vector2 ( bounds.center.x, bounds.min.y + rayMargin );
			endPoint = new Vector2 ( bounds.center.x, bounds.max.y - rayMargin );
			boundsMargin = bounds.size.x / 2f;
		}

		// y cast
		else {

			startPoint = new Vector2 ( bounds.min.x + rayMargin, bounds.center.y );
			endPoint = new Vector2 ( bounds.max.x - rayMargin, bounds.center.y );
			boundsMargin = bounds.size.y / 2f;
		}

		// reset supplied return params
		if ( hitInfos != null ) {
			hitInfos.Clear ();
		}

		// ray starts from collider center so add half of bounds size
		float castDistance = distance + boundsMargin;

		closestHitDistance = int.MaxValue;
		
		int totalNumHits = 0;

		// cast each ray
		for ( int i = 0; i < numRays; i++ ) {

			float lerpFactor = i / (float)(numRays - 1);

			Vector2 origin = Vector2.Lerp ( startPoint, endPoint, lerpFactor );

			int numHits = Physics2D.RaycastNonAlloc ( origin, direction, hitAlloc, castDistance, layerMask );
		
			// iterate over hit objects
			for ( int j = 0; j < numHits; j++ ) {

				RaycastHit2D hit = hitAlloc[j];

				if ( (ignoreTriggers && hit.collider.isTrigger) || (ignoreSelf && (hit.transform == this.transform || hit.transform == this.transform.parent)) ) 
					continue;

				// add to supplied list if doesn't already contain same objet
				if ( hitInfos != null && (hitInfos.FindIndex (hitInfo => hitInfo.collider == hit) == -1) ) {
					hitInfos.Add (hit);
				}

				// update closest hit
				float hitDistance = (castDistance * hit.fraction) - boundsMargin;
				closestHitDistance = Math.Min ( closestHitDistance, hitDistance );
			}

			totalNumHits += numHits;
		}

		return totalNumHits;
	}
}
