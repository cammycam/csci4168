﻿using UnityEngine;
using System.Collections;

public class LevelSelect : MonoBehaviour {
	public int numLevels = 0;
	public GUISkin customSkin;
	public Vector2 scrollPosition = Vector2.zero;

	int buttonSpace;
	int buttonW;
	int buttonH;
	int btnsPerColumn = 5;

	string unlockLevelWord = "demo";
	string typedWord = "";

	// Use this for initialization
	void Start () {
	}

	void Update(){
		buttonW = Screen.width/6;
		buttonH = Screen.height/8;
		buttonSpace = (Screen.width - (btnsPerColumn * buttonW)) / (btnsPerColumn + 1);

		if (Input.GetKeyDown ("d")) {
			typedWord += "d";
		}else if (Input.GetKeyDown ("e")) {
			typedWord += "e";
		}else if (Input.GetKeyDown ("m")) {
			typedWord += "m";
		}else if (Input.GetKeyDown ("o")) {
			typedWord += "o";
		}

		if (typedWord == unlockLevelWord) {
			UnlockedLevels.numUnlocked = numLevels;
		}

		if (typedWord.Length > unlockLevelWord.Length) {
			typedWord="";
		}
	}
	
	void OnGUI () {
		int colCounter = 1;
		int currX = buttonSpace, currY = Screen.height/4;
		GUI.skin = customSkin;

		int titleSize = Screen.height / 20;
		int textSize = Screen.height / 45;

		GUI.contentColor = Color.black;
		GUI.Label (new Rect (0, titleSize, Screen.width, Screen.height/3), "<size=" + titleSize + ">Level Select</size>");
		GUI.contentColor = Color.white;

		if(GUI.Button(new Rect(10,10,buttonW,buttonH), "<size=" + textSize + ">Back</size>")) {
			Application.LoadLevel("MainMenu");
		}

		if (numLevels > 20) {
			int scrollLength = (numLevels/btnsPerColumn) * (buttonH + buttonSpace/2) - 20;
			
			scrollPosition = GUI.BeginScrollView(new Rect (currX, Screen.height/4, Screen.width-currX, Screen.height* 3/4), scrollPosition, new Rect(currX, Screen.height/4, 0,scrollLength));
		}
		for (int i = 1; i <= numLevels; i++) {
			if(i <= UnlockedLevels.numUnlocked){
				GUI.enabled = true;
			}else{
				GUI.enabled = false;
			}
			if(GUI.Button(new Rect(currX,currY,buttonW,buttonH), "<size=" + textSize + ">Level " + i + "</size>")) {
				Application.LoadLevel("Level"+(i-1));
			}

			if(colCounter < btnsPerColumn){
				currX += buttonSpace + buttonW;
				colCounter++;
			}else{
				currX = buttonSpace;
				colCounter = 1;
				currY += buttonSpace * 2 + buttonSpace/2;
			}
		}
		if (numLevels > 20) {
			GUI.EndScrollView();
		}
	}
}
