﻿using UnityEngine;
using System.Collections;

public class PauseOnLevelStart : MonoBehaviour {

	public GUISkin guiSkin;
	
	protected float textWidth = 250f,
					textHeight = 250f,
					textFlickerTime = 0.75f;

	protected bool isTextEnabled;
	
	void OnEnable () {
		StartCoroutine (StartLevelAfterPlayerInput());
	}

	// disable after player presses space
	IEnumerator StartLevelAfterPlayerInput () {

		float nextFlickerTime = 0.0f;

		// track time cuz it's broken????
		float time = 0.0f;

		// game is paused until input
		while (!Input.GetKeyDown (KeyCode.Space)) {

			// flicker text
			if (time >= nextFlickerTime) {

				isTextEnabled = !isTextEnabled;
				nextFlickerTime = time + textFlickerTime;
			}

			time += Time.deltaTime;

			yield return null;
		}

		GameManager.BroadcastAll ("OnLevelStart");

		GameObject.Find ("MouseCursor").GetComponent<MouseCursor>().enabled = false;

		this.enabled = false;
	}
	
	void OnGUI () {

		if (isTextEnabled) {

			GUI.skin = guiSkin;
				
			GUI.Label (new Rect (Screen.width / 2f - textWidth / 2f, Screen.height / 2f - textHeight / 2f, textWidth, textHeight), "-Space to Start-");
		}
	}
}
