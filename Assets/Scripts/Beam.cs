﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(LineRenderer))]
public class Beam : MonoBehaviour {

	public bool fireOnLevelStart;

	public LayerMask layermask;

	protected LineRenderer lineRenderer;

	protected List<Vector3> verts;

	protected float offsetFromOrigin;

	protected List<Transform> prevCollisions,
							  currCollisions;

	protected virtual void Awake () {

		lineRenderer = GetComponent<LineRenderer>();

		verts = new List<Vector3>();

		prevCollisions = new List<Transform>();
		currCollisions = new List<Transform>();
	}

	protected void Start () {

		// offset origin so casts from slightly outside of own collider
		Collider2D col = (collider2D == null) ? transform.parent.collider2D : collider2D;
		
		if (col != null)
			offsetFromOrigin = col.bounds.size.x / 1.95f;
	}

	void OnEnable () {

		lineRenderer.enabled = true;

		prevCollisions.Clear ();
		currCollisions.Clear ();
	}

	void OnDisable () {
		
		lineRenderer.enabled = false;
	}

	void OnLevelStart () {

		if (fireOnLevelStart) {

			this.enabled = true;
		}
	}

	void OnLevelRestart () {
		
		this.enabled = false;
	}

	protected virtual void Update () {

		Ray2D ray = new Ray2D (transform.position + transform.right * offsetFromOrigin, transform.right);

		// start beam at ray origin
		verts.Clear ();
		verts.Add (ray.origin);

		// fill list with verts by raycasting
		CastRay (ray);

		UpdateLineRenderer ();

		// alert objects that are no longer hit by beam
		UpdateCollisions ();
	}

	protected void UpdateLineRenderer () {

		lineRenderer.SetVertexCount (verts.Count);

		for (int i = 0; i < verts.Count; i++) {
			lineRenderer.SetPosition (i, new Vector3 (verts[i].x, verts[i].y, -1));
		}
	}

	// raycast determines point of beam collision and draws line from origin
	// calls self recursively to add line segments for reflections
	protected void CastRay (Ray2D ray) {

		// prevent stack overflow when beam bounces back and forth forever
		if (verts.Count > 10) 
			return;

		float castDistance = 50f;
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, castDistance, layermask);

		if (hit.collider != null) {

			currCollisions.Add (hit.transform);

			if (!prevCollisions.Contains (hit.transform)) {
				AlertObjectOfCollision (hit);
			}

			// end line segment at collision point
			Vector2 collisionPoint = ray.origin + ray.direction * hit.fraction * castDistance;
			verts.Add (collisionPoint);

			// reflect beam if collided object is reflective
			if (hit.collider.CompareTag ("Reflective") || (hit.transform.parent != null && hit.transform.parent.CompareTag ("Reflective"))) {

				Vector2 reflectedRay = Vector3.Reflect (ray.direction, hit.normal.normalized);
				ray.direction = reflectedRay.normalized;
				ray.origin = collisionPoint + 0.1f * ray.direction;		// offset raycast origin to be outside of hit collider so same collision isn't triggered for new ray

				CastRay (ray);
			}
		}

		// end beam at ray endpoint
		else {

			Vector2 vert = ray.origin + ray.direction * castDistance;
			verts.Add (vert);
		}
	}

	protected void UpdateCollisions () {

		IEnumerable<Transform> notColliding = prevCollisions.Except (currCollisions);

		foreach (Transform obj in notColliding) {
			obj.SendMessage ("OnLaserExit", SendMessageOptions.DontRequireReceiver);
		}

		List<Transform> temp = prevCollisions;
		prevCollisions = currCollisions;
		currCollisions = temp;
		currCollisions.Clear ();
	}

	protected void AlertObjectOfCollision (RaycastHit2D hit) {

		hit.transform.SendMessage ("OnLaserEnter", SendMessageOptions.DontRequireReceiver);
	}
}
