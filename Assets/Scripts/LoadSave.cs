﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class LoadSave : MonoBehaviour {

	// Use this for initialization
	void Awake () {
	
		int num = 1;

		if (File.Exists (Application.dataPath + "/save.txt")) {

			num = Convert.ToInt32 (File.ReadAllText (Application.dataPath + "/save.txt"));
		}

		UnlockedLevels.numUnlocked = num;
	}
}
