﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	private AudioSource audioSource;

	private static MusicManager instance;

	// Use this for initialization
	void Awake () {

		// fix case of going back to main menu and more music managers trying to spawn
		if (instance == null) {
			instance = this;
		}

		else {
			Destroy (this.gameObject);
		}

		audioSource = GetComponent<AudioSource>();

		DontDestroyOnLoad (this.gameObject);
	}

	void OnLevelWasLoaded (int level) {

		if (GameObject.Find ("OceanBackground") && audioSource.clip.name != "Ocean") {

			audioSource.clip = Resources.Load ("Music/Ocean") as AudioClip;
			audioSource.Play ();
		}

		else if (GameObject.Find ("CityBackground") && audioSource.clip.name != "City") {
		
			audioSource.clip = Resources.Load ("Music/City") as AudioClip;
			audioSource.Play ();
		}

		else if (GameObject.Find ("BarnBackground") && audioSource.clip.name != "Farm") {

			audioSource.clip = Resources.Load ("Music/Farm") as AudioClip;
			audioSource.Play ();
		}

		else if (Application.loadedLevel == 0 && audioSource.clip.name != "Title") {
			
			audioSource.clip = Resources.Load ("Music/Title") as AudioClip;
			audioSource.Play ();
		}
	}
}
