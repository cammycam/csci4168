﻿using UnityEngine;
using System.Collections;

public interface IController {

	void Enable ();
	void Disable ();
}

public interface IDamageable {

	int HealthAmount { get; }

	void Damage (int damageAmount, Vector2 knockbackDirection, bool overrideInvulnerability = false);
}

public interface IProjectile {

	void Deflect (Transform deflectedBy);
}
