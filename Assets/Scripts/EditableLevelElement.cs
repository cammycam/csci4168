﻿using UnityEngine;
using System.Collections;

public class EditableLevelElement : MonoBehaviour {

	public bool canRotate = true;

	private static float dragSpeed = 12.5f,
				   		 rotateSpeed = 400.0f;

	private int sortingOrder;

	private bool isSelected,
				 isRotating,
				 isColliding,
				 wasTrigger;

	private Vector2 initialPosition;

	public bool IsRotating { 
		get { return isRotating; }
	}

	public bool IsColliding {
		get { return isColliding; }
	}

	void OnLevelStart () {

		// log where player drops element
		initialPosition = this.transform.position;

		GetComponent<SpriteRenderer>().material = Resources.Load ("Materials/BaseMat") as Material;
	}

	void OnLevelRestart () {

		// put level element where player dropped it on last attempt for convenience
		this.transform.position = initialPosition;

		GetComponent<SpriteRenderer>().material = Resources.Load ("Materials/EditableMat") as Material;
	}

	public void OnSelected () {
		
		isSelected = true;

		if (collider2D.isTrigger) {
			wasTrigger = true;
		}
		else {
			collider2D.isTrigger = true;
		}

		sortingOrder = renderer.sortingOrder;
		renderer.sortingOrder = 1000;
		renderer.material = Resources.Load ("Materials/SelectedMat") as Material;

		// if moving platform disable makeparentontriggerenter script while editing
		MakeParentOnTriggerEnter scr = GetComponent<MakeParentOnTriggerEnter>();
		if (scr) {
			scr.enabled = false;
		}
	}
	
	// drop object at supplied position w/ supplied rotation
	public void OnDeselected (Vector2 position, Quaternion rotation) {
		
		isSelected = false;

		if (!wasTrigger)
			collider2D.isTrigger = false;
		
		transform.position = position;
		transform.rotation = rotation;

		renderer.sortingOrder = sortingOrder;
		renderer.material = Resources.Load ("Materials/EditableMat") as Material;

		// if moving platform reenable makeparentontriggerenter script after editing
		MakeParentOnTriggerEnter scr = GetComponent<MakeParentOnTriggerEnter>();
		if (scr) {
			scr.enabled = false;
		}
	}

	// alert player cannot drop here
	void OnTriggerStay2D (Collider2D col) {

		if (isSelected && !isColliding) {

			isColliding = true;

			renderer.material.SetFloat ("_IsColliding", 1.0f);
		}
	}

	// okay to drop now
	void OnTriggerExit2D (Collider2D col) {
		
		if (isSelected && isColliding) {
			
			isColliding = false;

			renderer.material.SetFloat ("_IsColliding", 0.0f);
		}
	}

	// move in world-space
	public void Move (Vector2 direction) {

		Vector2 translation = direction * dragSpeed * Time.deltaTime;
		transform.Translate (transform.InverseTransformDirection (translation));
	}
	
	public IEnumerator Rotate (float angle) {

		isRotating = true;

		Quaternion targetRotation = Quaternion.AngleAxis (angle, Vector3.forward) * transform.rotation;
				
		while (transform.rotation != targetRotation) {

			Quaternion rotation = Quaternion.RotateTowards (transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
			transform.rotation = rotation;
			
			yield return null;
		}
		
		// snap rotation to multiple of 90
		Vector3 eulers = transform.eulerAngles;
		eulers.z = Mathf.Round (eulers.z * 90f) / 90f;

		transform.rotation = Quaternion.Euler (eulers);
		
		isRotating = false;
	}
}
