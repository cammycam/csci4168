﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class UnlockedLevels {

	public static int numUnlocked = 1;

	public static void UnlockLevel () {

		File.WriteAllText (Application.dataPath + "/save.txt", (++numUnlocked).ToString ());
	}
}
