﻿using UnityEngine;
using System.Collections;

public class MouseCursor : MonoBehaviour {

	//public float moveSpeed;

	public LayerMask layermask;

	private EditableLevelElement selectedObject;

	private Vector2 selectedObjectInitialPosition;
	private Quaternion selectedObjectInitialRotation;

	void Awake () {

		Screen.showCursor = true;

		this.transform.position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
	}

	void OnLevelStart () {

		this.enabled = false;
	}

	void OnLevelRestart () {

		this.enabled = true;
	}

	void OnDisable () {

		if (selectedObject != null) {
			DropSelectedObject (selectedObjectInitialPosition, selectedObjectInitialRotation);
		}
	}

	void Update () {
			
		// left click
		if (Input.GetMouseButtonDown(0)) {

			// check if object at mouse position
			if (selectedObject == null)
				CastRay ();

			// drop object at mouse position
			else
				DropSelectedObject (selectedObject.transform.position, selectedObject.transform.rotation);
		}

		// right click -- drop object where was picked up
		else if (Input.GetMouseButtonDown(1)) {
			DropSelectedObject (selectedObjectInitialPosition, selectedObjectInitialRotation);
		}

		// move cursor
		//Vector2 mouseDelta = new Vector2 (Input.GetAxis ("Mouse X"), Input.GetAxis ("Mouse Y"));
		//this.transform.Translate (mouseDelta * moveSpeed * Time.deltaTime);

		// move cursor
		this.transform.position = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		if (selectedObject != null) {
			UpdateSelectedObject ();
		}
	}

	void UpdateSelectedObject () {

		// rotate obj w/ keys
		if (Input.GetKeyDown (KeyCode.D) && !selectedObject.IsRotating) {
			StartCoroutine (selectedObject.Rotate (-90f));
		}

		if (Input.GetKeyDown (KeyCode.A) && !selectedObject.IsRotating && selectedObject.canRotate) {
			StartCoroutine (selectedObject.Rotate (90f));
		}
	}
	

	// picks up object if player clicked moveable object
	void CastRay () {

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity, layermask);

		if (hit.collider != null) {
			PickUpObject (hit.transform);
		}
	}

	void PickUpObject (Transform obj) {
		
		selectedObject = obj.GetComponentInChildren<EditableLevelElement>();
		
		if (selectedObject != null) {

			selectedObject.OnSelected ();

			selectedObjectInitialPosition = selectedObject.transform.position;
			selectedObjectInitialRotation = selectedObject.transform.rotation;

			selectedObject.transform.parent = this.transform;

			Screen.showCursor = false;
		}
	}

	void DropSelectedObject (Vector2 dropPosition, Quaternion dropRotation) {

		if (!selectedObject.IsColliding || (dropPosition == selectedObjectInitialPosition)) {

			selectedObject.OnDeselected (dropPosition, dropRotation);

			selectedObject.transform.parent = null;
			selectedObject = null;

			Screen.showCursor = true;
		}
	}
}
