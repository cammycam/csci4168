using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[RequireComponent (typeof(Collider2D))]
[RequireComponent (typeof(CollisionManager))]
public class PlatformerMovement : MonoBehaviour {

	[SerializeField]
	protected bool isAffectedByGravity,
				   resetXVelocityEachFrame;

	[SerializeField]
	protected float moveSpeed,
				    maxFallSpeed;
		
	protected CollisionManager collision;

	protected Vector2 velocity;

	protected bool isGrounded;

	public float MoveSpeed {
		get; set;
	}

	public bool IsGrounded { 
		get { return isGrounded; } 
	}

	public Vector2 Velocity { 
		get { return velocity; } 
		set { velocity = value; } 
	}

	void Awake () {

		collision = GetComponent<CollisionManager>();

		Velocity = new Vector2 (moveSpeed, 0f);
	}

	void OnLevelStart () {

		this.enabled = true;
	}

	void OnLevelRestart () {
		
		this.enabled = false;
	}

	// moves transform by current velocity if no collision
	// if collision in one direction, moves transform to point of collision
	void LateUpdate () {

		Bounds bounds = collider2D.bounds;
		float closestHitDistance;
		
		// check collision below
		if (velocity.y <= 0f) {

			float distanceToCheck = Mathf.Max (5, Mathf.Abs (velocity.y)) * Time.deltaTime;
			RaycastHit2D hit = collision.CheckCollision ( -Vector2.up, distanceToCheck, bounds, out closestHitDistance );

			if (hit.collider != null) {

				transform.Translate ( -Vector2.up * closestHitDistance, Space.World );
				velocity.y = 0f;

				if (!isGrounded) {

					isGrounded = true;
					gameObject.SendMessage ("OnGrounded", SendMessageOptions.DontRequireReceiver);
				}
			}

			else if (isGrounded) {

				isGrounded = false;
				gameObject.SendMessage ("OnAirborne", SendMessageOptions.DontRequireReceiver);
			}
		}

		// check collision above
		else if (velocity.y > 0f) {

			if (isGrounded) {

				isGrounded = false;
				gameObject.SendMessage ("OnAirborne", SendMessageOptions.DontRequireReceiver);
			}

			float distanceToCheck = velocity.y * Time.deltaTime;
			RaycastHit2D hit = collision.CheckCollision ( Vector2.up, distanceToCheck, bounds, out closestHitDistance );

			if (hit.collider != null) {

				transform.Translate ( Vector2.up * closestHitDistance, Space.World );
				velocity.y = 0f;
			}
		}

		// check x collision
		if (velocity.x != 0f) {
			
			float distanceToCheck = Math.Abs (velocity.x) * Time.deltaTime;
			Vector2 directionToCheck = Vector2.right * (velocity.x / Math.Abs (velocity.x));
			RaycastHit2D hit = collision.CheckCollision (directionToCheck, distanceToCheck, collider2D.bounds, out closestHitDistance);
			
			if (hit.collider != null) {

				transform.Translate ( directionToCheck * closestHitDistance, Space.World);
				velocity.x = 0;

				// turn around
				transform.Rotate (Vector3.up, 180.0f);
			}
		}

		// apply movement
		transform.Translate (velocity * Time.deltaTime, Space.World );

		// update y velocity due to gravity
		if (!isGrounded && isAffectedByGravity) {
			velocity.y = Mathf.Max ( velocity.y - (GameManager.Gravity * Time.deltaTime), -maxFallSpeed );
		}

		// zero out x velocity so lack of x input == 0
		if (resetXVelocityEachFrame) {
			velocity.x = 0f;
		}
	}

	public void Move (float xDirection) {

		// ensure normalized
		xDirection /= Math.Abs (xDirection);

		velocity.x = xDirection * moveSpeed;
	}
}