﻿using UnityEngine;
using System.Collections;

public class OnLevelLoad : MonoBehaviour {

	public GameObject[] prefabsToLoad;


	public void OnLevelWasLoaded (int level) {

		// destroy default camera
		GameObject oldCamera = GameObject.Find ("Main Camera");
		if (oldCamera != null) 
			DestroyImmediate (oldCamera);

		// load each prefab
		foreach (GameObject obj in prefabsToLoad) {

			// if doesn't already exist, instantiate prefab and remove "(Clone)" from name
			if (GameObject.Find(obj.name) == null) {

				GameObject instance = GameObject.Instantiate (obj) as GameObject;
				instance.name = instance.name.Substring (0, instance.name.IndexOf("(Clone)"));
			}
		}
	}
}
