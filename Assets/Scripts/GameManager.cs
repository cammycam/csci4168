﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	protected static GameManager instance;

	[SerializeField]
	protected float gravity;

	protected GameObject player;
	protected GameObject cursor;
	
	private float _gravity;

	private PauseOnLevelStart pauseScript;

	public static float Gravity {
		get { return instance._gravity; }
	}

	public static GameObject Player {
		get { return instance.player; }
	}

	void Awake () {

		if (instance == null) {

			instance = this;
			pauseScript = GetComponent<PauseOnLevelStart>();

			DontDestroyOnLoad (this.gameObject);

			Init ();
		}

		else {
			DestroyImmediate (this.gameObject);
		}
	}
		
	void Start () {

		instance = this;
		DontDestroyOnLoad (this.gameObject);
	}

	public static void LoadNextLevel () {

		int nextLevel = Application.loadedLevel+1;

		if (Application.loadedLevel > 2)
			UnlockedLevels.UnlockLevel();

		if (nextLevel < Application.levelCount) 
			Application.LoadLevel (Application.loadedLevel+1);

		// else beat game
		else {
			DestroyImmediate (GameObject.Find ("GameManager"));
			MainMenu ();
		}
	}

	void Init () {

		pauseScript.enabled = true;

		player = GameObject.FindGameObjectWithTag ("Player");

		_gravity = gravity;		// set private gravity value to value in inspector

		Screen.showCursor = true;		
	}

	void Update () {

		if (Input.GetKeyDown (KeyCode.R) && !pauseScript.enabled) {
			RestartLevel ();
		}

		else if (Input.GetKeyDown (KeyCode.Q)) {
			MainMenu ();
		}
	}

	public static void MainMenu () {

		Application.LoadLevel (1);

		if (instance != null)
			Destroy (instance.gameObject);
	}

	public static void RestartLevel () {

		instance.GetComponent<PauseOnLevelStart>().enabled = true;

		BroadcastAll ("OnLevelRestart");
	}

	void OnLevelWasLoaded (int level) {
		
		Init ();
	}

	public static void BroadcastAll(string methodName) {

		GameObject[] gos = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));

		foreach (GameObject go in gos) {
			if (go && go.transform.parent == null) {
				go.gameObject.BroadcastMessage(methodName, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}
