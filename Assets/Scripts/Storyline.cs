﻿using UnityEngine;
using System.Collections;

public class Storyline : MonoBehaviour {
	public GUISkin customSkin;
	public bool city;
	public bool farm;
	public bool ocean;
	public bool intro;
	public bool ending;
	public Texture panel;
	int textSize;

	void Start () {

		Camera.main.GetComponent<CameraMovement>().enabled = false;
	}

	// Update is called once per frame
	void Update () {
		textSize = Screen.height / 55;

		if (Input.anyKeyDown) {
			Camera.main.GetComponent<CameraMovement>().enabled = true;
			GameManager.LoadNextLevel ();
		}
	}

	void OnGUI (){
		GUI.skin = customSkin;
		customSkin.label.alignment = TextAnchor.MiddleLeft;
		GUI.DrawTexture (new Rect (Screen.width / 4, Screen.height / 8 - 5, Screen.width / 2, Screen.height - Screen.height*2/8 + 10), 
		                 panel, ScaleMode.StretchToFill);
		if (city) {
			GUI.Label (new Rect (Screen.width / 4 + Screen.width/20, Screen.height / 8 + 10, Screen.width / 2, Screen.height - Screen.height*2/8), 
			            "<size=" + textSize + ">The city is bustling down below\n" +
			           "People rushing, disregard for everyone else\n" +
			           "around them.\n" +
			           "What animals are there to save in the city?\n" +
			           "Well, rodents of course!\n" +
			           "Oh young shepherd,\n" +
			           "alleyway vermin, why save them?\n" +
			           "Because you're the Shepherd of Legend\n" +
			           "and that's what you do.\n" +
			           "No matter the animal, if they're in need\n" +
			           "you're there to guide them\n" +
			           "to safety.\n" +
			           "And these rats and mice are all too likely\n" +
			           "to die at the hands of poison or a vicious cat.\n" +
			           "So, young shepherd, do your thing,\n" +
			           "Lead them away from harm.\n" +
						"</size>");
		} else if (farm) {
			GUI.contentColor = Color.black;
			GUI.Label (new Rect (Screen.width / 4 + Screen.width/20, Screen.height / 8 + 10, Screen.width /2, Screen.height - Screen.height*2/8), 
			           "<size=" + textSize + ">This farm is sanctuary\n" +
			           "to the animals it houses.\n" +
			           "See the cows graze, oh young shepherd?\n" +
			           "Do you see them at peace?\n" +
			           "Well we'd like to keep it that way\n" +
			           "You, The Shepherd of Legend to not just\n" +
			           "aid when disasters strike, but to prevent them\n" +
			           "as well.\n" +
			           "A storm's approaching from the North East,\n" +
			           "Tremendous power in blades of wind.\n" +
			           "The farmer can't get his cattle back into their barns.\n" +
			           "And that's where you come in.\n" +
			           "Make sure they get back safely, before the storm hits.\n" +
			           "Do not let the sanctuary be disturbed. \n" +
			           "</size>");
		} else if (ocean){
			GUI.contentColor = Color.black;
			GUI.Label (new Rect (Screen.width / 4 + Screen.width/20, Screen.height / 8 + 10, Screen.width /2, Screen.height - Screen.height*2/8), 
			           "<size=" + textSize + ">Underwater paradise, corrupted by waste.\n" +
			           "An oil spill far off slowly approaches.\n" +
			           "It won't be long before it enshrouds these waters.\n" +
			           "The animals near the spill are already dead,\n" +
			           "the whales here oblivious\n" +
			           "of what's to come.\n" +
			           "You can still save them, young shepherd,\n" +
			           "but it won't be easy.\n" +
			           "See poachers have set up traps,\n" +
			           "that they'll fall into without fail\n" +
			           "unless guided around them.\n" +
			           "Are you up for the task?\n" +
			           "To fight both disaster and man?\n" +
			           "I hope so, or else these whales are damned.\n" +
			           "</size>");
		}else if(intro){
			GUI.contentColor = Color.black;
			GUI.Label (new Rect (Screen.width / 4 + Screen.width/20, Screen.height / 8 + 10, Screen.width /2, Screen.height - Screen.height*2/8), 
			           "<size=" + textSize + ">Welcome to a world of magic and adventure.\n" +
			           "A land with animals in great danger.\n" +
			           "Danger caused by disaster, poachers, \n" +
			           "predators and lasers.\n" +
			           "Lasers? Yes Lasers! Try to keep up.\n" +
			           "Pay attention, for you are their only hope.\n" +
			           "You are the Shepherd of Legend, you see?\n" +
			           "And with your powers you can help them flee.\n" +
			           "Your powers enable you to move the land,\n" +
			           "anything from nature, at your command.\n" +
			           "Manipulate the world to guide them to safety\n" +
			           "Failure to do so will end quite sadly.\n" +
			           "So what are you waiting for, go on, make haste.\n" +
			           "The animals need you, there's no time to waste!\n" +
			           "</size>");
		}else{
			GUI.Label (new Rect (Screen.width / 4 + Screen.width/20, Screen.height / 8 + 10, Screen.width /2, Screen.height - Screen.height*2/8), 
			           "<size=" + textSize + ">Well young shepherd, here we are.\n" +
			           "Your inexperience worried me at first\n" +
			           "but you proved my doubts wrong.\n" +
			           "From the ocean, to the city, to the countryside\n" +
			           "animals have been saved\n" +
			           "from the calamity of nature\n" +
			           "and the destructive habits of humans\n" +
			           "all thanks to you.\n" +
			           "You moved the land, made paths\n" +
			           "to lead the animals to safety.\n" +
			           "Your powers guided you, guided the animals.\n" +
			           "You truly are the Shepherd Of Legend,\n" +
			           "a beacon of hope and hero\n" +
			           "for all animals.\n" +
			           "</size>");
		}
		GUI.contentColor = Color.white;
		customSkin.label.alignment = TextAnchor.MiddleCenter;
	}
}
