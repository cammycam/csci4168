﻿using UnityEngine;
using System.Collections;

public class Whale : Friend {

	// Use this for initialization
	protected override void Start () {
		
		base.Start ();
	}
	
	void OnLevelStart () {
		
		StartCoroutine (Routine());
	}

	IEnumerator Routine () {

		while (enabled) {

			// always move forward
			movement.Move (transform.right.x);

			yield return null;
		}
	}
}
