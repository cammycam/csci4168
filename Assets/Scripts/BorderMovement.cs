﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
public class BorderMovement : PathMovement {

	void Start () {
	
		CreatePathAroundCollider (transform.parent.collider2D);

		Edge currentEdge = FindCurrentEdge ();
		StartCoroutine (TraversePath(currentEdge));
	}

	// continue in same direction around cyclic path
	protected override IEnumerator TraversePath (Edge currentEdge) {
		
		while (true) {
			
			// move to next vertex in current edge
			yield return StartCoroutine (TraverseEdge(currentEdge));
			
			// update current edge
			int currentEdgeIndex = edges.IndexOf (currentEdge);
			int nextEdgeIndex = (currentEdgeIndex + 1) % edges.Count;
			currentEdge = edges[nextEdgeIndex];
		}
	}

	public void CreatePathAroundCollider (Collider2D col) {

		points.Clear ();
		edges.Clear ();

		float xMin = col.bounds.min.x;
		float xMax = col.bounds.max.x;
		float yMin = col.bounds.min.y;
		float yMax = col.bounds.max.y;

		// store parent collider's vertices in parent's local space
		points.Add (new Point (col.transform.InverseTransformPoint (new Vector2 (xMin, yMin))));
		points.Add (new Point (col.transform.InverseTransformPoint (new Vector2 (xMin, yMax))));
		points.Add (new Point (col.transform.InverseTransformPoint (new Vector2 (xMax, yMax))));
		points.Add (new Point (col.transform.InverseTransformPoint (new Vector2 (xMax, yMin))));

		// create edges
		edges.Add (new Edge (points[0], points[1]));
		edges.Add (new Edge (points[1], points[2]));
		edges.Add (new Edge (points[2], points[3]));
		edges.Add (new Edge (points[3], points[0]));
	}

	public Edge FindCurrentEdge () {
		
		Point closestPoint = points[0];
		float closestDist = Mathf.Infinity;
		
		// get closest point
		foreach (Point p in points) {
			
			float dist = Vector2.Distance (p.position, transform.localPosition);
			
			if (dist < closestDist) {
				closestPoint = p;
				closestDist = dist;
			}
		}
		
		Edge currentEdge = closestPoint.edges[0];
		
		// test edges of closest point to find current edge
		foreach (Edge e in closestPoint.edges) {
			
			float xMin = Mathf.Min (e.p1.x, e.p2.x);
			float xMax = Mathf.Max (e.p1.x, e.p2.x);
			float yMin = Mathf.Min (e.p1.y, e.p2.y);
			float yMax = Mathf.Max (e.p1.y, e.p2.y);
			
			// if transform is between the x- or y-values of this edge, then this is current edge
			if ((transform.localPosition.x >= xMin && transform.localPosition.x <= xMax) || (transform.localPosition.y >= yMin && transform.localPosition.y <= yMax )) {
				currentEdge = e;
				break;
			}
		}
		
		return currentEdge;
	}
}
*/
