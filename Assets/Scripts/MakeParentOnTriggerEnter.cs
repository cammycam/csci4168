﻿using UnityEngine;
using System.Collections;

public class MakeParentOnTriggerEnter : MonoBehaviour {

	public new bool enabled;

	void OnTriggerStay2D (Collider2D col) {

		if (enabled && col.CompareTag ("Friend") && col.transform.parent != this.transform) {
			col.transform.parent = this.transform;
		}
	}

	void OnTriggerExit2D (Collider2D col) {

		if (enabled && col.CompareTag ("Friend")) {
			col.transform.parent = null;
		}
	}
}
