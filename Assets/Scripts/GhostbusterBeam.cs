﻿using UnityEngine;
using System.Collections;

public class GhostbusterBeam : Beam {

	public float maxRayOffset,
     			 forceFieldRadius;

	public int minRecursions,
			   maxRecursions,
			   minForceFieldVerts,
	maxForceFieldVerts;

	protected new void Start () {

		base.Start ();
	}

	protected override void Update () {

	}

	public void Init (Transform target) {

		this.enabled = true;
		lineRenderer.enabled = true;

		StartCoroutine (Fire (target));
	}

	public void End () {
		
		StopAllCoroutines ();
		lineRenderer.enabled = false;
	}

	protected IEnumerator Fire (Transform target) {

		Ray2D ray = new Ray2D ();

		while (true) {
		
			ray.origin = transform.position + transform.right * offsetFromOrigin;
			ray.direction = transform.right;

			// start beam at ray origin
			verts.Clear ();

			verts.Add (new Vector3 (ray.origin.x, ray.origin.y, -1));
			verts.Add (target.position);

			// woooo
			CreateFieldAroundTarget (target);

			// break up beam segments into smaller crazy segments
			Ghostbusterize ();
			
			// do it
			UpdateLineRenderer ();
			
			// alert objects that are no longer hit by beam
			UpdateCollisions ();

			yield return null;
		}
	}

	protected void CreateFieldAroundTarget (Transform target) {

		int numVerts = Random.Range (minForceFieldVerts, maxForceFieldVerts);
		
		for (int i = 0; i < numVerts; i++) {
			
			float randomAngle = Random.Range (0, 360);
			Vector3 vertOffset = new Vector3 (Mathf.Cos (randomAngle), Mathf.Sin (randomAngle), 0);
			vertOffset *= forceFieldRadius;
			vertOffset.z = -1;
			
			verts.Add (target.position + vertOffset);
		}
	}

	protected void Ghostbusterize (int vertStartIndex = 0) {
		
		Vector3 p1 = verts[vertStartIndex];
		Vector3 p2 = verts[vertStartIndex+1];
		
		int numBeamSegments = verts.Count - 1;
		
		for (int i = 0; i < numBeamSegments; i++) {
			
			int numLevels = (int) Mathf.Log (Vector3.Distance (p1, p2), 1.5f);
			numLevels = Mathf.Clamp (numLevels, minRecursions, maxRecursions);
			
			GhostbusterizeRec (p1, p2, numLevels);
			
			int p2Index = verts.IndexOf (p2);
			p1 = p2;
			
			if (p2Index+1 < verts.Count)
				p2 = verts[p2Index+1];
		}
	}

	// recursively breaks beam into offset segments
	// if given target generates crazy force field around it
	void GhostbusterizeRec (Vector3 p1, Vector3 p2, int levels) {
		
		if (levels <= 0) 
			return;
		
		Vector3 midpoint = Vector2.Lerp (p1, p2, 0.5f);
		midpoint.z = -1;
		
		midpoint.x += Random.Range (-maxRayOffset, maxRayOffset);
		midpoint.y += Random.Range (-maxRayOffset, maxRayOffset);

		verts.Insert (verts.IndexOf (p2), midpoint);
		
		GhostbusterizeRec (p1, midpoint, levels-1);
		GhostbusterizeRec (midpoint, p2, levels-1);
	}
}
