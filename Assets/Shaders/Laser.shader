﻿Shader "Custom/Laser" {
	Properties {
	
		_Color ("Color", Color) = (1,1,1,1)
	}
		
	SubShader {
		
		Tags { "RenderType"="Transparent" }
		
		PASS {
		
			BLEND SRCALPHA ONEMINUSSRCALPHA
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform float4 _Color;

			struct a2v {
			
				float4 vertex : POSITION;
			};
			
			struct v2f {
			
				float4 position : POSITION;
				float4 scrPos : TEXCOORD0;
			};
			
			v2f vert (a2v v) {
						
				v2f f;
				f.position = mul (UNITY_MATRIX_MVP, v.vertex);
				f.scrPos = ComputeScreenPos (f.position);
				
				return f;	
			}
			
			float4 frag (v2f f) : COLOR {
			
				float2 wPos = (f.scrPos.xy/f.scrPos.w);

				float4 c = float4 (_Color.rgb, 1);
				
				c.r += 0.3 * sin (100000 * cos (_Time.w * wPos.x)) + 0.3;
				c.r += 0.3 * cos (100000 * sin (_Time.w * wPos.y)) + 0.3;
				float g = 0.5 * cos (5000 * _Time.z) + 0.4;
				c.g = (g >= 0.875) ? 0.6 : 0;
				c.a = 0.5 * cos (sin (_Time.a)) + 0.5;

				return c;
			}
			
			ENDCG
		}
	}
}
