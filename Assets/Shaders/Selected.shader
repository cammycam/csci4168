﻿Shader "Custom/Selected" {
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_CollidingColor ("CollidingColor", Color) = (1,1,1,1)
		_Alpha ("Alpha", float) = 1.0
		[MaterialToggle] _IsColliding ("IsColliding", Float) = 0
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 1
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent+100" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;
			fixed4 _CollidingColor;
			float _Alpha;
			float _IsColliding;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord);
				
				// only modify non-transparent pixels of tex
				if (c.a > 0.01) {
					
					c.a = _Alpha;
					
					if (_IsColliding)
						c = _CollidingColor;
				}
		
				return c;
			}
		ENDCG
		}
	}
}
