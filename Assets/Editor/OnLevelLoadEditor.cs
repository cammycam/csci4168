﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(OnLevelLoad))]
public class OnLevelLoadEditor : Editor {
	
	public override void OnInspectorGUI () {

		OnLevelLoad levelLoader = (OnLevelLoad) target;

		DrawDefaultInspector ();

		if ( GUILayout.Button ("Load Level") ) {
			levelLoader.OnLevelWasLoaded (-1);
		}
	}
}
